package net.azeti.challenge.recipe.recipe.util.mapper;

import net.azeti.challenge.recipe.recipe.model.IngredientEntity;
import net.azeti.challenge.recipe.recipe.model.Recipe;
import net.azeti.challenge.recipe.recipe.model.RecipeEntity;
import net.azeti.challenge.recipe.recipe.model.Unit;
import net.azeti.challenge.recipe.recipe.util.exception.UnitNotRecognizedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RecipeMapperTest {
    private final static String ONE = "1.0";
    private final static String EGG = "egg";
    private final static String SPACE = " ";
    private final static String ONE_PC_EGG = "1.0 pc egg";
    private final static String ONE_X_EGG = "1.0 x egg";
    private final static String UNIT_NOT_RECOGNIZED_MESSAGE = " is not recognized";
    private final static String INVALID_UNIT_X = "x";

    private RecipeMapper recipeMapper;

    @BeforeEach
    void setUp() {
        recipeMapper = new RecipeMapper();
    }

    @Test
    void recipeToEntity_whenInputIsValid_ReturnRecipeEntity() {
        Recipe recipe = mockRecipe(ONE_PC_EGG);
        RecipeEntity entity = recipeMapper.recipeToEntity(recipe);
        assertEquals(recipe.getTitle(), entity.getTitle());
        assertEquals(recipe.getUsername(), entity.getUsername());
        assertEquals(recipe.getDescription(), entity.getDescription());
        assertEquals(recipe.getInstructions(), entity.getInstructions());
        assertEquals(recipe.getServings(), entity.getServings());
        assertEquals(1, entity.getIngredients().size());
        assertEquals(Double.valueOf(ONE), entity.getIngredients().get(0).getIngredientValue());
        assertEquals(Unit.PC, entity.getIngredients().get(0).getIngredientUnit());
        assertEquals(EGG, entity.getIngredients().get(0).getIngredientType());
    }

    @Test
    void recipeToEntity_whenUnitIsInvalid_ThrowUnitNotRecognizedException() {
        Recipe recipe = mockRecipe(ONE_X_EGG);
        Exception exception = assertThrows(UnitNotRecognizedException.class, () -> {
            recipeMapper.recipeToEntity(recipe);
        });
        assertEquals(UnitNotRecognizedException.class, exception.getClass());
        assertEquals(INVALID_UNIT_X+UNIT_NOT_RECOGNIZED_MESSAGE, exception.getMessage());
    }

    @Test
    void entityToRecipe_whenInputIsValid_ReturnRecipe() {
        RecipeEntity entity = mockRecipeEntity();
        Recipe recipe = recipeMapper.entityToRecipe(entity);
        assertEquals(entity.getTitle(), recipe.getTitle());
        assertEquals(entity.getUsername(), recipe.getUsername());
        assertEquals(entity.getDescription(), recipe.getDescription());
        assertEquals(entity.getInstructions(), recipe.getInstructions());
        assertEquals(entity.getServings(), recipe.getServings());
        assertEquals(1, recipe.getIngredients().size());
        assertEquals(ONE_PC_EGG, recipe.getIngredients().get(0));
    }

    private Recipe mockRecipe(String ingredient) {
        Recipe recipe = new Recipe();
        recipe.setTitle("title");
        recipe.setUsername("username");
        recipe.setDescription("description");
        recipe.setIngredients(List.of(ingredient));
        recipe.setInstructions("Instructions");
        recipe.setServings("4/6");
        return recipe;
    }

    private RecipeEntity mockRecipeEntity() {
        RecipeEntity recipeEntity = new RecipeEntity();
        recipeEntity.setTitle("title");
        recipeEntity.setUsername("username");
        recipeEntity.setDescription("description");
        recipeEntity.setIngredients(List.of(mockIngredientEntity(1.0, Unit.PC, EGG)));
        recipeEntity.setInstructions("Instructions");
        recipeEntity.setServings("4/6");
        return recipeEntity;
    }

    private IngredientEntity mockIngredientEntity(Double val, Unit unit, String type) {
        IngredientEntity ingredientEntity = new IngredientEntity();
        ingredientEntity.setIngredientValue(val);
        ingredientEntity.setIngredientUnit(unit);
        ingredientEntity.setIngredientType(type);
        return ingredientEntity;
    }
}