package net.azeti.challenge.recipe.recipe.service;

import net.azeti.challenge.recipe.recipe.model.IngredientEntity;
import net.azeti.challenge.recipe.recipe.model.Recipe;
import net.azeti.challenge.recipe.recipe.model.RecipeEntity;
import net.azeti.challenge.recipe.recipe.model.Unit;
import net.azeti.challenge.recipe.recipe.repository.IngredientRepository;
import net.azeti.challenge.recipe.recipe.repository.RecipeRepository;
import net.azeti.challenge.recipe.recipe.util.mapper.RecipeMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;


class RecipeManagementImplTest {

    @Mock
    private RecipeRepository recipeRepository;
    @Mock
    private IngredientRepository ingredientRepository;
    @Mock
    private RecipeMapper recipeMapper;

    @InjectMocks
    private RecipeManagementImpl recipeManagement;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void create_whenMandatoryAttributesAreGiven_ReturnRecipe() {
        Recipe recipe = mockRecipe();
        RecipeEntity recipeEntity = mockRecipeEntity();

        Mockito.when(recipeMapper.recipeToEntity(recipe)).thenReturn(recipeEntity);
        recipeEntity.setId(1l);
        Mockito.when(recipeRepository.save(recipeEntity)).thenReturn(recipeEntity);
        recipe.setId(1l);
        Mockito.when(recipeMapper.entityToRecipe(recipeEntity)).thenReturn(recipe);

        Recipe createdRecipe = recipeManagement.create(recipe);
        assertEquals(recipe, createdRecipe);
    }

    @Test
    void getById_whenResourceExist_ReturnRecipe() {
        Recipe recipe = mockRecipe();
        recipe.setId(1l);
        RecipeEntity recipeEntity = mockRecipeEntity();
        recipeEntity.setId(1l);

        Mockito.when(recipeRepository.findById(anyLong())).thenReturn(Optional.ofNullable(recipeEntity));
        Mockito.when(recipeMapper.entityToRecipe(recipeEntity)).thenReturn(recipe);

        Recipe foundRecipe = recipeManagement.getById(1l);
        assertEquals(recipe, foundRecipe);
    }

    @Test
    void getById_whenResourceDoesNotExist_ReturnNull() {
        Mockito.when(recipeRepository.findById(anyLong())).thenReturn(Optional.ofNullable(null));
        Mockito.when(recipeMapper.entityToRecipe(null)).thenReturn(null);

        Recipe foundRecipe = recipeManagement.getById(1l);
        assertNull(foundRecipe);
    }

    @Test
    void update_whenResourceExist_ReturnUpdatedRecipe() {
        RecipeEntity existingRecipeEntity = mockRecipeEntity();
        Recipe updatedRecipe = mockUpdatedRecipe();
        RecipeEntity updatedRecipeEntity = mockUpdatedRecipeEntity();

        Mockito.when(recipeRepository.findById(anyLong())).thenReturn(Optional.ofNullable(existingRecipeEntity));
        Mockito.when(recipeMapper.recipeToEntity(updatedRecipe)).thenReturn(updatedRecipeEntity);
        Mockito.when(recipeRepository.save(any(RecipeEntity.class))).thenReturn(updatedRecipeEntity);
        Mockito.when(recipeMapper.entityToRecipe(any(RecipeEntity.class))).thenReturn(updatedRecipe);

        Recipe recipe = recipeManagement.update(1l, updatedRecipe);
        assertEquals(updatedRecipe, recipe);

    }

    @Test
    void update_whenResourceDoesNotExist_ReturnNull() {
        Recipe recipe = mockRecipe();
        Mockito.when(recipeRepository.findById(anyLong())).thenReturn(Optional.ofNullable(null));

        Recipe updatedRecipe = recipeManagement.update(1l, recipe);
        assertNull(updatedRecipe);
    }

    @Test
    void delete_whenResourceExist_ReturnDeletedRecipe() {
        Recipe recipe = mockRecipe();
        recipe.setId(1l);
        RecipeEntity recipeEntity = mockRecipeEntity();
        recipeEntity.setId(1l);

        Mockito.when(recipeRepository.findById(anyLong())).thenReturn(Optional.ofNullable(recipeEntity));
        doNothing().when(recipeRepository).deleteById(anyLong());
        Mockito.when(recipeMapper.entityToRecipe(recipeEntity)).thenReturn(recipe);

        Recipe deletedRecipe = recipeManagement.delete(1l);
        assertNotNull(deletedRecipe);
        assertEquals(recipe, deletedRecipe);
    }

    @Test
    void delete_whenResourceDoesNotExist_ReturnNull() {
        Mockito.when(recipeRepository.findById(anyLong())).thenReturn(Optional.ofNullable(null));

        Recipe deletedRecipe = recipeManagement.delete(1l);
        assertNull(deletedRecipe);
    }

    @Test
    void getByUsername_whenResourceExist_ReturnRecipes() {
        Recipe recipe = mockRecipe();
        recipe.setId(1l);
        RecipeEntity recipeEntity = mockRecipeEntity();
        recipeEntity.setId(1l);

        Mockito.when(recipeRepository.findByUsername(recipe.getUsername())).thenReturn(List.of(recipeEntity));
        Mockito.when(recipeMapper.entityToRecipe(recipeEntity)).thenReturn(recipe);

        List<Recipe> foundRecipes = recipeManagement.getByUser(recipe.getUsername());
        assertTrue(foundRecipes.size() == 1);
        assertEquals(recipe.getUsername(), foundRecipes.get(0).getUsername());
    }

    @Test
    void getByUsername_whenResourceDoesNotExist_ReturnEmptyList() {
        Recipe recipe = mockRecipe();
        recipe.setId(1l);

        Mockito.when(recipeRepository.findByUsername(recipe.getUsername())).thenReturn(Collections.emptyList());

        List<Recipe> foundRecipes = recipeManagement.getByUser(recipe.getUsername());
        assertTrue(foundRecipes.size() == 0);
    }

    @Test
    void searchRecipes_whenUsernameGiven_ReturnRecipes() {
        Recipe recipe = mockRecipe();
        recipe.setId(1l);
        RecipeEntity recipeEntity = mockRecipeEntity();
        recipeEntity.setId(1l);

        Mockito.when(recipeRepository.findByUsername(anyString())).thenReturn(List.of(recipeEntity));
        Mockito.when(recipeMapper.entityToRecipe(recipeEntity)).thenReturn(recipe);

        List<Recipe> foundRecipes = recipeManagement.getByUser(recipe.getUsername().substring(0,3));
        assertTrue(foundRecipes.size() == 1);
    }

    @Test
    void searchRecipes_whenUsernameGiven_ReturnEmptyList() {
        Recipe recipe = mockRecipe();

        Mockito.when(recipeRepository.findByUsername(anyString())).thenReturn(Collections.emptyList());

        List<Recipe> foundRecipes = recipeManagement.getByUser(recipe.getUsername().substring(0,3));
        assertTrue(foundRecipes.size() == 0);
    }

    @Test
    void searchRecipes_whenTitleGiven_ReturnRecipes() {
        Recipe recipe = mockRecipe();
        recipe.setId(1l);
        RecipeEntity recipeEntity = mockRecipeEntity();
        recipeEntity.setId(1l);

        Mockito.when(recipeRepository.findByUsername(anyString())).thenReturn(List.of(recipeEntity));
        Mockito.when(recipeMapper.entityToRecipe(recipeEntity)).thenReturn(recipe);

        List<Recipe> foundRecipes = recipeManagement.getByUser(recipe.getTitle().substring(1,3));
        assertTrue(foundRecipes.size() == 1);
    }

    @Test
    void searchRecipes_whenTitleGiven_ReturnEmptyList() {
        Recipe recipe = mockRecipe();

        Mockito.when(recipeRepository.findByUsername(anyString())).thenReturn(Collections.emptyList());

        List<Recipe> foundRecipes = recipeManagement.getByUser(recipe.getTitle().substring(1,3));
        assertTrue(foundRecipes.size() == 0);
    }

    private Recipe mockRecipe() {
        Recipe recipe = new Recipe();
        recipe.setTitle("title");
        recipe.setUsername("username");
        recipe.setDescription("description");
        recipe.setIngredients(List.of("1 pc egg", "0.5 l water"));
        recipe.setInstructions("Instructions");
        recipe.setServings("4/6");
        return recipe;
    }

    private Recipe mockUpdatedRecipe() {
        Recipe recipe = new Recipe();
        recipe.setTitle("title1");
        recipe.setUsername("username1");
        recipe.setDescription("description1");
        recipe.setIngredients(List.of("2 pc egg", "0.5 l water"));
        recipe.setInstructions("Instructions1");
        recipe.setServings("6/8");
        return recipe;
    }

    private RecipeEntity mockRecipeEntity() {
        RecipeEntity recipeEntity = new RecipeEntity();
        recipeEntity.setTitle("title");
        recipeEntity.setUsername("username");
        recipeEntity.setDescription("description");
        recipeEntity.setIngredients(List.of(mockIngredientEntity(1.0, Unit.PC, "egg"), mockIngredientEntity(0.5,
                Unit.L, "water")));
        recipeEntity.setInstructions("Instructions");
        recipeEntity.setServings("4/6");
        return recipeEntity;
    }

    private RecipeEntity mockUpdatedRecipeEntity() {
        RecipeEntity recipeEntity = new RecipeEntity();
        recipeEntity.setTitle("title1");
        recipeEntity.setUsername("username1");
        recipeEntity.setDescription("description1");
        recipeEntity.setIngredients(List.of(mockIngredientEntity(2.0, Unit.PC, "egg"), mockIngredientEntity(0.5,
                Unit.L, "water")));
        recipeEntity.setInstructions("Instructions1");
        recipeEntity.setServings("6/8");
        return recipeEntity;
    }

    private IngredientEntity mockIngredientEntity(Double val, Unit unit, String type) {
        IngredientEntity ingredientEntity = new IngredientEntity();
        ingredientEntity.setIngredientValue(val);
        ingredientEntity.setIngredientUnit(unit);
        ingredientEntity.setIngredientType(type);
        return ingredientEntity;
    }
}