package net.azeti.challenge.recipe.recipe.controller;

import net.azeti.challenge.recipe.recipe.model.Recipe;
import net.azeti.challenge.recipe.recipe.service.RecipeManagement;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;

class RecipeManagementApiTest {

    @Mock
    private RecipeManagement recipeManagementService;
    @InjectMocks
    private RecipeManagementApi recipeManagementApi;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void createRecipe_WhenMandatoryAttributesGiven_ReturnCreatedResponse() {
        Recipe recipe = mockRecipe();

        Mockito.when(recipeManagementService.create(recipe)).thenReturn(recipe);

        ResponseEntity<Recipe> response = recipeManagementApi.createRecipe(recipe);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    void createRecipe_WhenUsernameNotGiven_ThrowMethodArgumentNotValidException() {
       /* Recipe recipe = mockRecipe();
        recipe.setUsername(null);
        recipe.setTitle(null);

        Exception exception = assertThrows(MethodArgumentNotValidException.class, () -> {
            recipeManagementApi.createRecipe(recipe);
        });

        assertEquals(MethodArgumentNotValidException.class, exception.getClass());*/
    }

    @Test
    void getRecipeById_WhenValidIdGiven_ReturnOkResponse() {
        Recipe recipe = mockRecipe();
        recipe.setId(1l);

        Mockito.when(recipeManagementService.getById(1l)).thenReturn(recipe);

        ResponseEntity<Recipe> response = recipeManagementApi.getRecipeById(1l);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void getRecipeById_WhenInvalidIdGiven_ReturnNotFoundResponse() {
        Mockito.when(recipeManagementService.getById(1l)).thenReturn(null);

        ResponseEntity<Recipe> response = recipeManagementApi.getRecipeById(1l);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void updateRecipe_WhenValidInputGiven_ReturnOkResponse() {
        Recipe updatedRecipe = mockRecipe();
        updatedRecipe.setId(1l);
        updatedRecipe.setTitle("Updated Title");
        Mockito.when(recipeManagementService.update(1l, updatedRecipe)).thenReturn(updatedRecipe);

        ResponseEntity<Recipe> response = recipeManagementApi.updateRecipe(1l, updatedRecipe);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void updateRecipe_WhenInvalidInputGiven_ReturnNotFoundResponse() {
        Recipe updatedRecipe = mockRecipe();
        updatedRecipe.setId(1l);
        updatedRecipe.setTitle("Updated Title");
        Mockito.when(recipeManagementService.update(anyLong(), any(Recipe.class))).thenReturn(null);

        ResponseEntity<Recipe> response = recipeManagementApi.updateRecipe(2l, updatedRecipe);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void deleteRecipe_WhenValidIdGiven_ReturnNoContentResponse() {
        Recipe deletedRecipe = mockRecipe();
        deletedRecipe.setId(1l);
        Mockito.when(recipeManagementService.delete(1l)).thenReturn(deletedRecipe);

        ResponseEntity<Object> response = recipeManagementApi.deleteRecipe(1l);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void deleteRecipe_WhenInvalidIdGiven_ReturnNotFoundResponse() {
        Mockito.when(recipeManagementService.delete(anyLong())).thenReturn(null);

        ResponseEntity<Object> response = recipeManagementApi.deleteRecipe(1l);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void getRecipeByUsername_WhenValidInputGiven_ReturnOkResponse() {
        Recipe recipe = mockRecipe();
        recipe.setId(1l);
        Mockito.when(recipeManagementService.getByUser(recipe.getUsername())).thenReturn(List.of(recipe));

        ResponseEntity<List<Recipe>> response = recipeManagementApi.getRecipeByUsername(recipe.getUsername());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void getRecipeByUsername_WhenInvalidInputGiven_ReturnNotFoundResponse() {
        Recipe recipe = mockRecipe();
        recipe.setId(1l);
        Mockito.when(recipeManagementService.getByUser(anyString())).thenReturn(Collections.emptyList());

        ResponseEntity<List<Recipe>> response = recipeManagementApi.getRecipeByUsername("user");
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void searchRecipes_WhenValidUsernameGiven_ReturnOkResponse() {
        Recipe recipe = mockRecipe();
        recipe.setId(1l);
        String partialUsername = recipe.getUsername().substring(1,3);
        Mockito.when(recipeManagementService.searchRecipes(partialUsername, null)).thenReturn(List.of(recipe));

        ResponseEntity<List<Recipe>> response = recipeManagementApi.searchRecipes(partialUsername, null);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void searchRecipes_WhenInvalidUsernameGiven_ReturnNotFoundResponse() {
        Recipe recipe = mockRecipe();
        recipe.setId(1l);
        String partialUsername = "abc";
        Mockito.when(recipeManagementService.searchRecipes(partialUsername, null)).thenReturn(Collections.emptyList());

        ResponseEntity<List<Recipe>> response = recipeManagementApi.searchRecipes(partialUsername, null);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void searchRecipes_WhenValidTitleGiven_ReturnOkResponse() {
        Recipe recipe = mockRecipe();
        recipe.setId(1l);
        String partialTitle = recipe.getTitle().substring(1,3);
        Mockito.when(recipeManagementService.searchRecipes(null, partialTitle)).thenReturn(List.of(recipe));

        ResponseEntity<List<Recipe>> response = recipeManagementApi.searchRecipes(null, partialTitle);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void searchRecipes_WhenInvalidTitleGiven_ReturnNotFoundResponse() {
        Recipe recipe = mockRecipe();
        recipe.setId(1l);
        String partialTitle = "abc";
        Mockito.when(recipeManagementService.searchRecipes(null, partialTitle)).thenReturn(Collections.emptyList());

        ResponseEntity<List<Recipe>> response = recipeManagementApi.searchRecipes(null, partialTitle);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    private Recipe mockRecipe() {
        Recipe recipe = new Recipe();
        recipe.setTitle("title");
        recipe.setUsername("username");
        recipe.setDescription("description");
        recipe.setIngredients(List.of("1 pc egg", "0.5 l water"));
        recipe.setInstructions("Instructions");
        recipe.setServings("4/6");
        return recipe;
    }
}