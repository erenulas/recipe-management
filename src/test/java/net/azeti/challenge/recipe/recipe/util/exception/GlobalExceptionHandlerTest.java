package net.azeti.challenge.recipe.recipe.util.exception;

import net.azeti.challenge.recipe.recipe.model.ErrorResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;

import static org.junit.jupiter.api.Assertions.*;

class GlobalExceptionHandlerTest {

    private final static String MANDATORY_FIELD_ERROR_MESSAGE = "Mandatory fields are missing";

    private GlobalExceptionHandler globalExceptionHandler;

    @BeforeEach
    void setUp() {
        globalExceptionHandler = new GlobalExceptionHandler();
    }

    @Test
    void handleValidationException_WhenMethodArgumentNotValidExceptionIsThrown() {
        MethodArgumentNotValidException ex = Mockito.mock(MethodArgumentNotValidException.class);
        ResponseEntity<ErrorResponse> errorResponse = globalExceptionHandler.handleValidationException(ex);
        assertEquals(HttpStatus.BAD_REQUEST, errorResponse.getStatusCode());
        assertEquals(MANDATORY_FIELD_ERROR_MESSAGE, errorResponse.getBody().getMessage());
    }

    @Test
    void testHandleValidationException_WhenUnitNotRecognizedExceptionIsThrown() {
        UnitNotRecognizedException ex = new UnitNotRecognizedException("Unit is not recognized");
        ResponseEntity<ErrorResponse> errorResponse = globalExceptionHandler.handleValidationException(ex);
        assertEquals(HttpStatus.BAD_REQUEST, errorResponse.getStatusCode());
        assertEquals(ex.getMessage(), errorResponse.getBody().getMessage());
    }
}