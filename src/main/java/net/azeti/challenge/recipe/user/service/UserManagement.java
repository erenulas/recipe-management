package net.azeti.challenge.recipe.user.service;

import net.azeti.challenge.recipe.user.model.Login;
import net.azeti.challenge.recipe.user.model.Registration;
import net.azeti.challenge.recipe.user.model.RegistrationResult;
import net.azeti.challenge.recipe.user.model.Token;

public interface UserManagement {

    RegistrationResult register(Registration registration);

    Token login(Login login);
}
