package net.azeti.challenge.recipe.user.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class Login {
    @NotNull
    private String username;
    @NotNull
    private String password;
}
