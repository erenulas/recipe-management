package net.azeti.challenge.recipe.user.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistrationResult {

    private String username;

}
