package net.azeti.challenge.recipe.user.service;

import net.azeti.challenge.recipe.user.model.*;
import net.azeti.challenge.recipe.user.repository.UserRepository;
import net.azeti.challenge.recipe.user.util.JWTTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserManagementImpl implements UserManagement {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JWTTokenUtil jwtTokenUtil;

    @Override
    public RegistrationResult register(Registration registration) {
        UserEntity userEntity = new UserEntity();
        userEntity.setUsername(registration.getUsername());
        userEntity.setEmail(registration.getEmail());
        userEntity.setPassword(passwordEncoder.encode(registration.getPassword()));
        userRepository.save(userEntity);
        RegistrationResult registrationResult = new RegistrationResult();
        registrationResult.setUsername(userEntity.getUsername());
        return registrationResult;
    }

    @Override
    public Token login(Login login) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(login.getUsername(), login.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String generatedToken = jwtTokenUtil.generateToken(authentication.getName());
        Token token = new Token();
        token.setAccessToken(generatedToken);
        return token;
    }
}
