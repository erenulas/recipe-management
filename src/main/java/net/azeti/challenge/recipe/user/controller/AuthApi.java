package net.azeti.challenge.recipe.user.controller;

import net.azeti.challenge.recipe.user.model.RegistrationResult;
import net.azeti.challenge.recipe.user.model.Login;
import net.azeti.challenge.recipe.user.model.Registration;
import net.azeti.challenge.recipe.user.model.Token;
import net.azeti.challenge.recipe.user.service.UserManagementImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
@Validated
public class AuthApi {

    @Autowired
    private UserManagementImpl userService;

    @PostMapping("/login")
    public Token login(@Valid @RequestBody Login login) {
        return userService.login(login);
    }

    @PostMapping("/users")
    public ResponseEntity<RegistrationResult> register(@Valid @RequestBody Registration registration) {
        RegistrationResult registrationResult = userService.register(registration);
        return new ResponseEntity<>(registrationResult, HttpStatus.CREATED);
    }
}
