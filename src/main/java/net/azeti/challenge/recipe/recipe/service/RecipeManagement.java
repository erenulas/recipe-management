package net.azeti.challenge.recipe.recipe.service;

import net.azeti.challenge.recipe.recipe.model.Recipe;

import java.util.List;

public interface RecipeManagement {

    Recipe create(Recipe recipe);

    Recipe getById(Long id);

    Recipe update(Long id, Recipe recipe);

    Recipe delete(Long id);

    List<Recipe> getByUser(String username);

    List<Recipe> searchRecipes(String username, String title);
}
