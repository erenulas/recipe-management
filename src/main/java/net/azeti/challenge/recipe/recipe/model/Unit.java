package net.azeti.challenge.recipe.recipe.model;

public enum Unit {
    G("g", "Gram"),
    KG("kg", "Kilogram"),
    ML("ml", "Milliliter"),
    L("l", "Liter"),
    PC("pc", "Piece"),
    TSP("tsp", "Teaspoon"),
    TBSP("tbsp", "Tablespoon"),
    PINCH("pinch", "A dash");

    private String unit;
    private String comment;

    Unit(String unit, String comment) {
        this.unit = unit;
        this.comment = comment;
    }

    public String getUnit() {
        return unit;
    }

    public String getComment() {
        return comment;
    }
}
