package net.azeti.challenge.recipe.recipe.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;


@Getter
@Setter
public class Recipe {
    private Long id;
    @NotNull
    private String title;
    @NotNull
    private String username;
    private String description;
    @NotNull
    private List<String> ingredients;
    @NotNull
    private String instructions;
    private String servings;
}
