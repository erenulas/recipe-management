package net.azeti.challenge.recipe.recipe.repository;

import net.azeti.challenge.recipe.recipe.model.RecipeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipeRepository extends JpaRepository<RecipeEntity, Long> {
    List<RecipeEntity> findByUsername(String username);
    List<RecipeEntity> findByUsernameContainingIgnoreCase(String username);
    List<RecipeEntity> findByTitleContainingIgnoreCase(String title);
}