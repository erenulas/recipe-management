package net.azeti.challenge.recipe.recipe.controller;

import net.azeti.challenge.recipe.recipe.model.Recipe;
import net.azeti.challenge.recipe.recipe.service.RecipeManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@Validated
public class RecipeManagementApi {

    @Autowired
    RecipeManagement recipeManagementService;

    @PostMapping("/recipes")
    public ResponseEntity<Recipe> createRecipe(@Valid @RequestBody Recipe recipe) {
        Recipe createdRecipe = recipeManagementService.create(recipe);
        ResponseEntity<Recipe> response = new ResponseEntity<>(createdRecipe, HttpStatus.CREATED);
        return response;
    }

    @GetMapping("/recipes/{id}")
    public ResponseEntity<Recipe> getRecipeById(@PathVariable Long id) {
        Recipe recipe = recipeManagementService.getById(id);
        HttpStatus status = recipe == null ? HttpStatus.NOT_FOUND : HttpStatus.OK;
        ResponseEntity<Recipe> response = new ResponseEntity<>(recipe, status);
        return response;
    }

    @PutMapping("/recipes/{id}")
    public ResponseEntity<Recipe> updateRecipe(@PathVariable Long id, @Valid @RequestBody Recipe updatedRecipe) {
        Recipe recipe =  recipeManagementService.update(id, updatedRecipe);
        HttpStatus status = recipe == null ? HttpStatus.NOT_FOUND : HttpStatus.OK;
        ResponseEntity<Recipe> response = new ResponseEntity<>(recipe, status);
        return response;
    }

    @DeleteMapping("recipes/{id}")
    public ResponseEntity<Object> deleteRecipe(@PathVariable Long id) {
        Recipe deletedRecipe = recipeManagementService.delete(id);
        HttpStatus status = deletedRecipe == null ? HttpStatus.NOT_FOUND : HttpStatus.NO_CONTENT;
        return ResponseEntity.status(status).build();
    }

    @GetMapping("/users/{username}/recipes")
    public ResponseEntity<List<Recipe>> getRecipeByUsername(@PathVariable String username) {
        List<Recipe> recipes = recipeManagementService.getByUser(username);
        HttpStatus status = recipes.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK;
        ResponseEntity<List<Recipe>> response = new ResponseEntity<>(recipes, status);
        return response;
    }

    @GetMapping("/recipes")
    public ResponseEntity<List<Recipe>> searchRecipes(
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "title", required = false) String title) {

        List<Recipe> recipes = recipeManagementService.searchRecipes(username, title);
        HttpStatus status = recipes.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK;
        ResponseEntity<List<Recipe>> response = new ResponseEntity<>(recipes, status);
        return response;
    }

    //TODO: no content  --- DONE
    //TODO: handle error cases --- DONE ---- remained authentication ones
    //TODO: handle mandatory attributes   --- DONE
    //TODO: when updating ensure that new ingredients contain recipe info ---- DONE
    //TODO: metric system validator --- DONE
    //TODO: api documentation and testing --- IN PROGRESS --- testing auth part remains, documentation update needed
    //TODO: notnull validation not working


}
