package net.azeti.challenge.recipe.recipe.util.mapper;

import net.azeti.challenge.recipe.recipe.model.IngredientEntity;
import net.azeti.challenge.recipe.recipe.model.Recipe;
import net.azeti.challenge.recipe.recipe.model.RecipeEntity;
import net.azeti.challenge.recipe.recipe.model.Unit;
import net.azeti.challenge.recipe.recipe.util.exception.UnitNotRecognizedException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RecipeMapper {

    public RecipeEntity recipeToEntity(Recipe recipe) {
        RecipeEntity entity = new RecipeEntity();
        entity.setTitle(recipe.getTitle());
        entity.setUsername(recipe.getUsername());
        entity.setDescription(recipe.getDescription());
        entity.setInstructions(recipe.getInstructions());
        entity.setServings(recipe.getServings());
        entity.setIngredients(convertStringToIngredientEntity(recipe, entity));

        return entity;
    }

    private List<IngredientEntity> convertStringToIngredientEntity(Recipe recipe, RecipeEntity entity) {
        List<IngredientEntity> ingredientEntities = new ArrayList<>();
        if (recipe.getIngredients() != null) {
            for (String ingredientText : recipe.getIngredients()) {
                IngredientEntity ingredientEntity = parseIngredientText(ingredientText);
                ingredientEntity.setRecipe(entity);
                ingredientEntities.add(ingredientEntity);
            }
        }
        return ingredientEntities;
    }

    private IngredientEntity parseIngredientText(String ingredientText) {
        IngredientEntity ingredientEntity = new IngredientEntity();

        String[] parts = ingredientText.split(" ", 3);
        if (parts.length >= 1) {
            String quantity = parts[0];
            ingredientEntity.setIngredientValue(Double.valueOf(quantity));
        }
        if (parts.length >= 2) {
            String unit = parts[1];
            try {
                ingredientEntity.setIngredientUnit(Unit.valueOf(unit.toUpperCase()));
            } catch (IllegalArgumentException exception) {
                throw new UnitNotRecognizedException(unit + " is not recognized");
            }
        }
        if (parts.length >= 3) {
            String type = parts[2];
            ingredientEntity.setIngredientType(type);
        }
        return ingredientEntity;
    }

    public Recipe entityToRecipe(RecipeEntity entity) {
        if(entity == null) {
            return null;
        }
        Recipe recipe = new Recipe();
        recipe.setId(entity.getId());
        recipe.setTitle(entity.getTitle());
        recipe.setUsername(entity.getUsername());
        recipe.setDescription(entity.getDescription());
        recipe.setInstructions(entity.getInstructions());
        recipe.setServings(entity.getServings());
        recipe.setIngredients(convertIngredientEntityToString(entity));
        return recipe;
    }

    private List<String> convertIngredientEntityToString(RecipeEntity entity) {
        List<String> ingredients = new ArrayList<>();
        if (entity.getIngredients() != null) {
            for (IngredientEntity ingredientEntity : entity.getIngredients()) {
                String ingredientText = buildIngredientText(ingredientEntity);
                ingredients.add(ingredientText);
            }
        }
        return ingredients;
    }

    private String buildIngredientText(IngredientEntity ingredientEntity) {
        StringBuilder ingredientText = new StringBuilder();
        if (ingredientEntity.getIngredientValue() != null) {
            ingredientText.append(ingredientEntity.getIngredientValue()).append(" ");
        }
        if (ingredientEntity.getIngredientUnit() != null) {
            ingredientText.append(ingredientEntity.getIngredientUnit().getUnit()).append(" ");
        }
        if (ingredientEntity.getIngredientType() != null) {
            ingredientText.append(ingredientEntity.getIngredientType());
        }
        return ingredientText.toString().trim();
    }
}
