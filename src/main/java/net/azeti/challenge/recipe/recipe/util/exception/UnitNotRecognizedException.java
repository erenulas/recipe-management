package net.azeti.challenge.recipe.recipe.util.exception;

public class UnitNotRecognizedException extends RuntimeException {

    public UnitNotRecognizedException(String message) {
        super(message);
    }
}