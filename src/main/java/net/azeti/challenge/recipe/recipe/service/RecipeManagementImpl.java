package net.azeti.challenge.recipe.recipe.service;

import net.azeti.challenge.recipe.recipe.model.IngredientEntity;
import net.azeti.challenge.recipe.recipe.model.Recipe;
import net.azeti.challenge.recipe.recipe.model.RecipeEntity;
import net.azeti.challenge.recipe.recipe.repository.IngredientRepository;
import net.azeti.challenge.recipe.recipe.repository.RecipeRepository;
import net.azeti.challenge.recipe.recipe.util.mapper.RecipeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class RecipeManagementImpl implements RecipeManagement{

    @Autowired
    RecipeRepository recipeRepository;
    @Autowired
    IngredientRepository ingredientRepository;
    @Autowired
    RecipeMapper recipeMapper;

    @Override
    public Recipe create(Recipe recipe) {
        RecipeEntity recipeEntity = recipeMapper.recipeToEntity(recipe);
        recipeRepository.save(recipeEntity);
        return recipeMapper.entityToRecipe(recipeEntity);
    }

    @Override
    public Recipe getById(Long id) {
        RecipeEntity entity = recipeRepository.findById(id).orElse(null);
        return recipeMapper.entityToRecipe(entity);
    }

    @Override
    public Recipe update(Long id, Recipe updatedRecipe) {
        RecipeEntity existingRecipe = recipeRepository.findById(id).orElse(null);
        if (existingRecipe != null) {
            RecipeEntity updatedRecipeEntity = recipeMapper.recipeToEntity(updatedRecipe);
            existingRecipe.setTitle(updatedRecipeEntity.getTitle());
            existingRecipe.setUsername(updatedRecipeEntity.getUsername());
            existingRecipe.setDescription(updatedRecipeEntity.getDescription());
            updateIngredients(existingRecipe, updatedRecipeEntity);
            existingRecipe.setInstructions(updatedRecipeEntity.getInstructions());
            existingRecipe.setServings(updatedRecipeEntity.getServings());
            recipeRepository.save(existingRecipe);
            return recipeMapper.entityToRecipe(existingRecipe);
        }
        return null;
    }

    private void updateIngredients(RecipeEntity existingRecipe, RecipeEntity updatedRecipe) {
        List<IngredientEntity> existingIngredients = existingRecipe.getIngredients();
        List<IngredientEntity> newIngredients = updatedRecipe.getIngredients();

        Map<String, IngredientEntity> existingIngredientMap = existingIngredients.stream()
                .collect(Collectors.toMap(IngredientEntity::getIngredientType, Function.identity()));

        for (IngredientEntity newIngredient : newIngredients) {
            IngredientEntity existingIngredient = existingIngredientMap.get(newIngredient.getIngredientType());

            if (existingIngredient != null) {
                existingIngredient.setIngredientUnit(newIngredient.getIngredientUnit());
                existingIngredient.setIngredientType(newIngredient.getIngredientType());
            } else {
                newIngredient.setRecipe(existingRecipe);
                existingIngredients.add(newIngredient);
            }
        }
    }

    @Override
    public Recipe delete(Long id) {
        RecipeEntity entity = recipeRepository.findById(id).orElse(null);
        if(entity != null) {
            recipeRepository.deleteById(id);
            return recipeMapper.entityToRecipe(entity);
        }
        return null;
    }

    @Override
    public List<Recipe> getByUser(String username) {
        List<RecipeEntity> entities = recipeRepository.findByUsername(username);
        List<Recipe> recipes = new ArrayList<>();
        entities.forEach(entity -> { recipes.add(recipeMapper.entityToRecipe(entity));});
        return recipes;
    }

    @Override
    public List<Recipe> searchRecipes(String username, String title) {
        List<RecipeEntity> entities = new ArrayList<>();
        if (username != null) {
            entities = recipeRepository.findByUsernameContainingIgnoreCase(username);
        } else if (title != null) {
            entities = recipeRepository.findByTitleContainingIgnoreCase(title);
        }
        List<Recipe> recipes = new ArrayList<>();
        entities.forEach(entity -> { recipes.add(recipeMapper.entityToRecipe(entity));});
        return recipes;
    }
}
