Recipe Management Application
-----------------------------

This application provides endpoints to create, get, update, delete, and search recipes.
It is created using Java 11, Spring Boot, Maven, JUnit, Mockito, and H2 Database.

Unit tests that cover the use cases of these endpoints and underlying services are implemented.

How to run the application
-----------------------------
- Import the project in IDE.
- Click on the Run button.
- Do requests to the following endpoints.

API Documentation
-----------------------------
**POST /api/recipes**

You can send a recipe containing title (required), username (required), description, ingredients (required), 
instructions (required), servings to create a recipe in the system
- If a required field is not provided, then bad request response is returned.

Valid Request Structure Example:
```json
{
  "title": "title",
  "username": "username",
  "description": "description",
  "ingredients": ["1 pc egg", "0.5 l water"],
  "instructions": "instructions",
  "servings": "servings"
}
```  

Success Response Structure Example:
```json
{
  "id": 1,
  "title": "title",
  "username": "username",
  "description": "description",
  "ingredients": [
    "1 pc egg",
    "0.5 l water"
  ],
  "instructions": "instructions",
  "servings": "servings"
}
```

Invalid Request Structure Example:
```json
{
  "username": "username",
  "description": "description",
  "ingredients": ["1 pc egg", "0.5 l water"],
  "instructions": "instructions",
  "servings": "servings"
}
```  

Error Response Structure Example:
```json
{
  "message": "Mandatory fields are missing"
}
```

**GET /api/recipes/{id}**

You can send id of the recipe that you want to retrieve. 
- If a recipe with the provided id exist, then endpoint OK response is returned.
- If a recipe with the provided id does not exist, then NOT FOUND response is returned.

Valid Request
-> GET /api/recipes/1

Success Response Structure Example:
```json
{
  "id": 1,
  "title": "title",
  "username": "username",
  "description": "description",
  "ingredients": [
    "1 pc egg",
    "0.5 l water"
  ],
  "instructions": "instructions",
  "servings": "servings"
}
```

Request with an id that does not exist
-> GET /api/recipes/2

Error Response:
HTTP Status 404 Not Found is returned.

**PUT /api/recipes/{id}**

You can send the id of the recipe that you want to update as path parameter and updated recipe data in the body of the request.
- If a required field is not provided, then bad request response is returned.
- If a recipe with the provided id exist, then the recipe is updated and OK response is returned.
- If a recipe with the provided id does not exist, then NOT FOUND response is returned.


Valid Request Structure Example:
-> PUT /api/recipes/1
```json
{
  "title": "new title",
  "username": "username",
  "description": "description",
  "ingredients": ["1 pc egg", "0.5 l water"],
  "instructions": "instructions",
  "servings": "servings"
}
```

Success Response Structure Example:
```json
{
  "id": 1,
  "title": "new title",
  "username": "username",
  "description": "description",
  "ingredients": [
    "1 pc egg",
    "0.5 l water"
  ],
  "instructions": "instructions",
  "servings": "servings"
}
```

Request with an id that does not exist
-> PUT /api/recipes/2
```json
{
  "title": "new title",
  "username": "username",
  "description": "description",
  "ingredients": ["1 pc egg", "0.5 l water"],
  "instructions": "instructions",
  "servings": "servings"
}
```

Error Response: 
HTTP Status 404 Not Found is returned.

Invalid Request Structure Example:
```json
{
  "username": "username",
  "description": "description",
  "ingredients": ["1 pc egg", "0.5 l water"],
  "instructions": "instructions",
  "servings": "servings"
}
```  

Error Response Structure Example:
```json
{
  "message": "Mandatory fields are missing"
}
```

**DELETE /api/recipes/{id}**

You can send the id of the recipe that you want to delete as path parameter.
- If a recipe with the provided id exist, then the recipe is deleted and NO CONTENT response is returned.
- If a recipe with the provided id does not exist, then NOT FOUND response is returned.


Valid Request Structure Example:
-> DELETE /api/recipes/1

Success Response:
HTTP Status 204 No Content is returned.

Request with an id that does not exist
-> DELETE /api/recipes/2

Error Response:
HTTP Status 404 Not Found is returned.

**GET /api/users/{username}/recipes**

You can send the username as path parameter to retrieve the recipes belonging to that user.
- If there are recipes belonging to the provided username, then endpoint OK response is returned.
- If there are no recipes belonging to the provided username, then NOT FOUND response is returned.

Valid Request
-> GET /api/users/john/recipes

Success Response Structure Example:
```json
[ 
  {
    "id": 1,
    "title": "title",
    "username": "john",
    "description": "description",
    "ingredients": [
      "1 pc egg",
      "0.5 l water"
    ],
    "instructions": "instructions",
    "servings": "servings"
  }
]
```

Request with an id that does not exist
-> GET /api/users/john.d/recipes

Error Response:
HTTP Status 404 Not Found is returned.

**GET /api/recipes?username=<username>&title=<title>**

You can send the username or title as query parameters to retrieve the recipes containing the provided values. 
Search can be done either by username or title and not with both. If both values are provided, 
search is done using the username and matching can be partial or exact.
- If there are recipes belonging to the provided values, then endpoint OK response is returned.
- If there are no recipes belonging to the provided values, then NOT FOUND response is returned.

Valid request to search by username
-> GET /api/recipes?username=joh

Success Response Structure Example:
```json
[
  {
    "id": 1,
    "title": "title",
    "username": "john",
    "description": "description",
    "ingredients": [
      "1 pc egg",
      "0.5 l water"
    ],
    "instructions": "instructions",
    "servings": "servings"
  }
]
```

Valid request to search by title
-> GET /api/recipes?title=tit

Success Response Structure Example:
```json
[
  {
    "id": 1,
    "title": "title",
    "username": "john",
    "description": "description",
    "ingredients": [
      "1 pc egg",
      "0.5 l water"
    ],
    "instructions": "instructions",
    "servings": "servings"
  }
]
```

Request with partial username that does not exist
-> GET /api/recipes?username=geo

Error Response:
HTTP Status 404 Not Found is returned.

Request with partial username that does not exist
-> GET /api/recipes?title=geo

Error Response:
HTTP Status 404 Not Found is returned.

**POST /api/users**

You can send your username, password, and email in the request body to register.
- If registration is successful, then CREATED response is returned.

Valid Request Structure Example:
```json
{
  "email": "john@mail.com",
  "username": "john",
  "password": "password"
}
``` 

Success Response Structure Example:
```json
{
  "username": "john"
}
``` 

**POST /api/login**

You can send your username and password in the request body to log in.
- If user is logged in successfully, then endpoint OK response and an access token is returned.
- If user is not logged in successfully, then FORBIDDEN response is returned without body.
    
Request with wrong password example:
```json
{
  "username": "john",
  "password": "password"
}
``` 

Success Response Structure Example:
```json
{
  "accessToken": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqb2huIiwiaWF0IjoxNjg2Nzc3ODgyLCJleHAiOjE2ODY3Nzg4ODJ9.6ovmiDMOVOJ1vy1s38sl9FG16X3bL8XR97kh-2wqeZA"
}
``` 

Request with wrong password example:
```json
{
  "username": "john",
  "password": "password1"
}
``` 

Error Response:
HTTP Status 403 Forbidden is returned.